package gui;

import generation.Distance;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class WallFollower implements RobotDriver{

	
	private BasicRobot robot; //private robot reference
	private Distance dist; 
	
	private int width, height;
	private float startingBattery;
	
	boolean[] currentSensors = {true, true, true, true}; //LEFT RIGHT FORWARD BACKWARD
	
	Controller controller;
	
	public WallFollower(){
	}
	
	public boolean[] getCurrentSensors(){
		return currentSensors;
	}
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;		
	}

	/**Assigns values to the internal attributes for width and height.
	 * @param width, width of the maze 0 < width < width_of_maze
	 * @param height, height of the maze 0 < height < height_of_maze
	 */
	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
		
	}

	/**Assigns the distance object to the robot driver. This is used by the wizzard driver to determine the shortest path.
	 */
	@Override
	public void setDistance(Distance distance) {
		dist = robot.maze.getMazedists();
		
	}

	/**This triggers the driver to refresh their understanding of the current sensor situation. If sensors have been repaired then they will use them again.
	 * 
	 */
	@Override
	public void triggerUpdateSensorInformation() {
		for(Direction d : Direction.values()){
			currentSensors[d.ordinal()] = robot.hasOperationalSensor(d);
		}
		
	}

	/**Method unique to the WallFollower class. This method relies on the distanceToObstacle method from the robot class. Rather tha ncalling it directly, there 
	 * is additional code in this method to detect faulty sensors and then rotate the robot extra times to get sensor readings. This makes traveling in the maze very costly but
	 * the left wall following still works with the left sensor disabled. 
	 */
	public int lookAtWall(Direction d){
		this.triggerUpdateSensorInformation();
		if(currentSensors[d.ordinal()]){
			return this.robot.distanceToObstacle(d);
		}
		else{ //if your current sensor is down then where do you look?
			switch(d){ //current sensors : LEFT // RIGHT // FORWARDS // BACKWARDS
			case BACKWARD:
				if(currentSensors[0]){
					this.robot.rotate(Turn.LEFT);
					int returnValue = this.robot.distanceToObstacle(Direction.LEFT);
					this.robot.rotate(Turn.RIGHT);
					return returnValue;
				}
				else if(currentSensors[1]){
					this.robot.rotate(Turn.RIGHT);
					int returnValue = this.robot.distanceToObstacle(Direction.RIGHT);
					this.robot.rotate(Turn.LEFT);
					return returnValue;
				}
				else if(currentSensors[2]){
					this.robot.rotate(Turn.AROUND);
					int returnValue = this.robot.distanceToObstacle(Direction.FORWARD);	
					this.robot.rotate(Turn.AROUND);
					return returnValue;
				}
				break;
			case FORWARD:
				if(currentSensors[0]){
					this.robot.rotate(Turn.RIGHT);
					int returnValue = this.robot.distanceToObstacle(Direction.LEFT);
					this.robot.rotate(Turn.LEFT);
					return returnValue;
				}
				else if(currentSensors[1]){
					this.robot.rotate(Turn.LEFT);
					int returnValue = this.robot.distanceToObstacle(Direction.RIGHT);
					this.robot.rotate(Turn.RIGHT);
					return returnValue;
				}
				else if(currentSensors[3]){
					this.robot.rotate(Turn.AROUND);
					int returnValue = this.robot.distanceToObstacle(Direction.BACKWARD);	
					this.robot.rotate(Turn.AROUND);
					return returnValue;
				}
				break;
			case LEFT:
				if(currentSensors[2]){
					this.robot.rotate(Turn.LEFT);
					int returnValue = this.robot.distanceToObstacle(Direction.FORWARD);
					this.robot.rotate(Turn.RIGHT);
					return returnValue;
				}
				else if(currentSensors[3]){
					this.robot.rotate(Turn.RIGHT);
					int returnValue = this.robot.distanceToObstacle(Direction.BACKWARD);	
					this.robot.rotate(Turn.LEFT);
					return returnValue;
				}
				else if(currentSensors[1]){
					this.robot.rotate(Turn.AROUND);
					int returnValue = this.robot.distanceToObstacle(Direction.RIGHT);
					this.robot.rotate(Turn.AROUND);
					return returnValue;
				}
				break;
			case RIGHT:
				if(currentSensors[2]){
					this.robot.rotate(Turn.RIGHT);
					int returnValue = this.robot.distanceToObstacle(Direction.FORWARD);
					this.robot.rotate(Turn.LEFT);
					return returnValue;
				}
				else if(currentSensors[3]){
					this.robot.rotate(Turn.LEFT);
					int returnValue = this.robot.distanceToObstacle(Direction.BACKWARD);	
					this.robot.rotate(Turn.RIGHT);
					return returnValue;
				}
				else if(currentSensors[0]){
					this.robot.rotate(Turn.AROUND);
					int returnValue = this.robot.distanceToObstacle(Direction.LEFT);
					this.robot.rotate(Turn.AROUND);
					return returnValue;
				}
				break;
			}
			return 10;
		}
	}
	
	
	
	/**The actual rules that the robot driver follows to reach the end of the maze. The wall follower is pretty straightforward. 
	 * While you are not at the exit, the robot has not stopped, and the game is still being played:
	 * Walk along the left wall. 
	 * If you hit a wall then turn right.
	 * If you lose the left wall, then turn left and walk forward.
	 * @exception if there is not enough battery 
	 * @return true if the maze is completed, false otherwise.
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		startingBattery = this.robot.getBatteryLevel();
		int wallDist = -1;
		while((this.robot.hasStopped() == false)&&(this.robot.isAtExit() == false)&&(controller.currentState == controller.states[2])){	//while the robot is not at the exit, go there
			System.out.println("Solving...");
			if(this.lookAtWall(Direction.LEFT) == 0){
				wallDist = this.lookAtWall(Direction.FORWARD);
				if(wallDist == 0){
					robot.rotate(Turn.RIGHT);
				}
				else{
					while((this.lookAtWall(Direction.LEFT) == 0)&&(wallDist > 0)){
						robot.move(1, false);
						wallDist--;
					}
				}
			}
			else{
				this.robot.rotate(Turn.LEFT);
				this.robot.move(1, false);
			}
		if(robot.hasStopped()){
			throw new Exception("Robot has run out of Energy!");
		}
		}
		
		//Post loop conditions for final returns
		if(this.robot.hasStopped){
			throw new Exception("Robot has no Energy!");
		}
		if(this.robot.isAtExit()){
			System.out.println("Finished!");
			return true;
		}
		else{
			System.out.println("Failed to complete maze...");
			return false;}
	}

	/**Fetches the total ammount of energy that was used from the driver
	 * @return float, the ammount of energy that was used.
	 */
	@Override
	public float getEnergyConsumption() {
		return this.startingBattery - robot.getBatteryLevel();
	}

	/**Fetches the path length traveled by the driver.
	 * @return int representing the distance traveled.
	 */
	@Override
	public int getPathLength() {
		return this.robot.getOdometerReading();
	}

	@Override
	public void setController(Controller controller) {
		this.controller = controller;
		
	}

}
