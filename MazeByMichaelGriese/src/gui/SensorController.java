package gui;

import gui.Robot.Direction;
/**@author Michael Griese
 * This class is meant to disrupt the robot during runtime. The robot will have sensors disabled at a regualar interval. 
 */
public class SensorController implements Runnable {

	Robot robot;
	Controller controller;
	
	boolean isDamaged = false;
	
	public SensorController(){
	}
	
	public void setRobot(Robot robot){
		this.robot = robot;
	}
	
	public boolean getDamage(){
		return this.isDamaged;
	}
	
	public void setController(Controller controller){
		this.controller = controller;
	} 
	@Override
	public void run() {
		while(true){
			if(this.robot == null){
				continue; //if there is no robot reference yet then do nothing
			}
			boolean[] stableList = controller.getStableList();			//LEFT//RIGHT //FORWARD//BACKWRAD
			if(!stableList[0]){
				robot.triggerSensorFailure(Direction.LEFT);
			}
			if(!stableList[1]){
				robot.triggerSensorFailure(Direction.RIGHT);
			}
			if(!stableList[2]){
				robot.triggerSensorFailure(Direction.FORWARD);
			}
			if(!stableList[3]){
				robot.triggerSensorFailure(Direction.BACKWARD);
			}
			try {
				isDamaged = true;
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				//do nothing idk what this does 
			}
			if(!stableList[0]){
				robot.repairFailedSensor(Direction.LEFT);
			}
			if(!stableList[1]){
				robot.repairFailedSensor(Direction.RIGHT);
			}
			if(!stableList[2]){
				robot.repairFailedSensor(Direction.FORWARD);
			}
			if(!stableList[3]){
				robot.repairFailedSensor(Direction.BACKWARD);
			}
			try {
				isDamaged = false;
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				//do nothing idk what this does 
			}
		}
	}
}
