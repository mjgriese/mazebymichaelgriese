package gui;

import generation.CardinalDirection;
import generation.Distance;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class Wizard implements RobotDriver{

	private BasicRobot robot; //private robot reference
	private Distance dist; 
	
	private float startingBattery;
	private int width, height;
	
	private Controller controller;
	
	boolean[] currentSensors = {true, true, true, true}; //LEFT RIGHT FORWARD BACKWARD

	public Wizard(){}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;		
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width; 
		this.height = height;
		
	}

	@Override
	public void setDistance(Distance distance) {
		dist = robot.maze.getMazedists();
		
	}

	@Override
	public void triggerUpdateSensorInformation() {
		for(Direction d : Direction.values()){
			currentSensors[d.ordinal()] = robot.hasOperationalSensor(d);
		}
		
	}

	@Override
	public boolean drive2Exit() throws Exception {
		boolean nextJump = false;
		Maze maze = controller.getMazeConfiguration();
		startingBattery = this.robot.getBatteryLevel();
		int wallDist = -1;
		while((this.robot.hasStopped() == false)&&(this.robot.isAtExit() == false)&&(controller.currentState == controller.states[2])){	//while the robot is not at the exit, go there
			System.out.println("Solving...");
			int[] cords = this.robot.getCurrentPosition();
			int[] nextMove = maze.getNeighborCloserToExit(cords[0], cords[1]);
			//add dist comparison here with wall shits
			try {
				if (dist.getDistanceValue(cords[0] - 1, cords[1]) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6){
					nextMove[0] = cords[0] - 1;
					nextMove[1] = cords[1];
					nextJump = true;
				}
				else if (dist.getDistanceValue(cords[0] + 1, cords[1]) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6){
					nextMove[0] = cords[0] + 1;
					nextMove[1] = cords[1];
					nextJump = true;
				}
				else if (dist.getDistanceValue(cords[0], cords[1] - 1) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6){
					nextMove[0] = cords[0];
					nextMove[1] = cords[1] - 1;
					nextJump = true;
				}
				else if (dist.getDistanceValue(cords[0], cords[1] + 1) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6){
					nextMove[0] = cords[0];
					nextMove[1] = cords[1] + 1;
					nextJump = true;
				}
			} catch (Exception e) {
				
			}
			//end compairisons 
			CardinalDirection currentDirection = this.robot.getCurrentDirection();
			CardinalDirection desiredDirection = CardinalDirection.getDirection(nextMove[0]-cords[0], nextMove[1]-cords[1]);
			if(currentDirection == desiredDirection){
				if(nextJump){
					this.robot.jump();
				}
				else{
					this.robot.move(1, false);
				}
			} 
			else{
				switch(currentDirection){
				case East:
					switch(desiredDirection){
					case North:
						this.robot.rotate(Turn.RIGHT);
						break;
					case South:
						this.robot.rotate(Turn.LEFT);
						break;
					case West:
						this.robot.rotate(Turn.AROUND);
						break;
					default:
						break;
					}
					break;
				case North:
					switch(desiredDirection){
					case East:
						this.robot.rotate(Turn.LEFT);
						break;
					case South:
						this.robot.rotate(Turn.AROUND);
						break;
					case West:
						this.robot.rotate(Turn.RIGHT);
						break;
					default:
						break;
					}
					break;
				case South:
					switch(desiredDirection){
					case East:
						this.robot.rotate(Turn.RIGHT);
						break;
					case North:
						this.robot.rotate(Turn.AROUND);
						break;
					case West:
						this.robot.rotate(Turn.LEFT);
						break;
					default:
						break;
					}					
					break;
				case West:
					switch(desiredDirection){
					case East:
						this.robot.rotate(Turn.AROUND);
						break;
					case South:
						this.robot.rotate(Turn.RIGHT);
						break;
					case North:
						this.robot.rotate(Turn.LEFT);
						break;
					default:
						break;
					}					
					break;
				default:
					break;
				}//large switch breaks 
				if(nextJump){
					this.robot.jump();
				}
				else{
					this.robot.move(1, false);
				}
			}
		}
		if(robot.hasStopped()){
			System.out.println("Robot Stopped");
		}
		
		if(robot.isAtExit()){
			System.out.println("Maze Completed!");
		}
	return false;
	}
	
	@Override
	public float getEnergyConsumption() {
		return this.startingBattery - robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	
	@Override
	public void setController(Controller controller) {
		this.controller = controller;
		
	}

}
