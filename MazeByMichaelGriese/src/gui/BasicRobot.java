package gui;

import generation.CardinalDirection;
import generation.Maze;
import generation.Floorplan;
import gui.Constants.UserInput;

/**BasicRobot class implements the robot interface and allows for remote/automatic completion of the maze.
 * Colaborates with Controler to simulate keypersses and move within the maze
 * Colaborates with StatePlaying to handle battery decreases as well as odometer increases when moving
 * @author Michael Griese 
 */

public class BasicRobot implements Robot{
	
	private float battery; //holds a float value that drains as robot is used
	private int odometer; //holds total distance traveled
	private int[] directionSensors; //array showing which directional sensors are active(1) or inactive(0) in order: North/South/East/West
	protected Controller controller; //how the robot sees the maze
	protected Maze maze;
	private Floorplan floor;
	protected boolean roomSensor;
	protected boolean exitSensor;
	protected boolean hasStopped;
	
	/**Method for instanciating the robot object. The robot recieves 3000 battery, a room sensor, a wall sensor on all sides, and an exit sensor. 
	 * The odometer is also reset to 0.
	 * @return Robot object
	 */
	public BasicRobot(){
		battery = 3000.0F;
		this.resetOdometer();
		directionSensors = new int[]{1, 1, 1, 1}; //backward - forward - left - right (-1) means no sensor / (0) means inactive sensor / (1) means active sensor
		roomSensor = true;
		exitSensor = true;
	}
	
	/**Method for getting the current coordinates of the robot within the maze. 
	 * @return int[] cordinateArray . .  cordinateArray[0] is the X value and cordinateArray[1] is the Y value
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {
		int[] cordinateArray = controller.getCurrentPosition(); 
		if((0 <= cordinateArray[0])&&(cordinateArray[0] < maze.getWidth())){ //if true then current X is inside the maze
			if((0 <= cordinateArray[1])&&(cordinateArray[1] < maze.getHeight())){ //if true then Y is inside the maze
				return cordinateArray;
			}
		}
		throw new Exception("Position outside of maze");
	}

	/**Returns the current cardinal direction that the robot is facing. The direction is not locally stored but rather taken from the controller.
	 * @return CardinalDirection current direction that the robot is facing.
	 */
	@Override
	public CardinalDirection getCurrentDirection() {
		return controller.getCurrentDirection();
	}

	/**Method associates the robot to the current controler as well as the maze and floorplan it is operaing within. This is perhaps not the cleanest implementation but 
	 * it is functional.
	 * @param Controller controller - the controller object that the robot wil use to traverse the maze.
	 */
	@Override
	public void setMaze(Controller controller) {
		this.controller = controller;
		this.maze = controller.getMazeConfiguration();
		this.floor = maze.getFloorplan();
	}

	/**Method to access the current value of the battery level.
	 * @return float battery - current battery level of the robot.
	 */
	@Override
	public float getBatteryLevel() {
		return battery;
	}

	/**Method to change the battery level of the robot. Used by StatePlaying to change the battery level whenever a move is completed.
	 * @param float level, this is the value that battery will be set to. (remaining battery level)
	 */
	@Override
	public void setBatteryLevel(float level) {
		battery = level;		
	}
	
	/**Used by StatePlaying to increment the odometer becuase it is a private class attribute. 
	 */
	@Override 
	public void incrementOdometer(){
		this.odometer++;
	}
	
	/**Method returns the current odometer reading of the robot, this indicates the distance travled by the robot.
	 * @return int value that is the path length the robot has moved.
	 */
	@Override
	public int getOdometerReading() {
		return odometer;
	}

	/**This method resets the internal odometer of the robot to zero. 
	 */
	@Override
	public void resetOdometer() {
		odometer = 0;
	}
	
	/**Method used by StatePlaying to determine the energy costs for movement.
	 * @return float value of the cost to rotate 360 degrees in the maze.
	 */
	@Override
	public float getEnergyForFullRotation() {
		return 12.0F; //based on documentation provided
	}

	/**Method used by StatePlaying to determine the energy costs for movement.
	 * @return float value of the cost to move one unit in the maze.
	 */
	@Override
	public float getEnergyForStepForward() {
		return 5.0F; //based on documentation provided
	}
	
	/**Method retuns if the robot is currently at the exit position
	 * @return boolean true if the robot is currently at an exit position, flase if it is not currently at the exit.
	 */
	@Override
	public boolean isAtExit() {
		int[] cords;
		try {
			cords = this.getCurrentPosition();
			} 
			catch (Exception e) {
				return false;
			}
		return floor.isExitPosition(cords[0], cords[1]);
	}

	/** Method determines if the robot can see through the exit to the area outside the maze in a given direcion.
	 * @param Direction is the relative direction to the robots current perspective to check.
	 * @exeption UnsupportedOperationException is raised if there is no room sensor in the given direction
	 */
	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		int dist = this.distanceToObstacle(direction);
		if(dist < 0)
			return true;
		return false;
	}
 
	/**Method determines if the robot is currently inside a room or not. 
	 * @return True if the robot is inside a room, false if it is not inside a room
	 * @exception UnsupportedOperationException is raised if the robot does not have a room sensor insalled.
	 */
	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		if(!(this.hasRoomSensor())){
			throw new UnsupportedOperationException("This robot has no room sensor!");
		}
		int[] cords; //attempt to get current position in maze
		try {
			cords = this.getCurrentPosition();
			} 
			catch (Exception e) {
				return false;
			}
		return floor.isInRoom(cords[0], cords[1]);
	}

	/**Returns the value of roomSensor, the ability for the robot to check if it is in a room.
	 */
	@Override
	public boolean hasRoomSensor() {
		return roomSensor;
	}

	/**Returns the value stored in hasStopped. hasStopped indicates the robot is out of energy or has hit a wall in automatic dirver mode.
	 */
	@Override
	public boolean hasStopped() {
		return hasStopped;
	}

	/**This method sets the value of hasStopped, it is called by the controller in stateplaying when the robot does not have enough energy to continue.
	 * @param stopValue boolean vlaue that is stored in hasStopped. indicates the robot is out of energy or has hit a wall in automatic dirver mode.
	 */
	@Override
	public void setHasStopped(boolean stopValue){
		this.hasStopped = stopValue;
	}
	
	/**This method is the obstacle detection that detects how far a wall is in a given direcion. 
	 * @exception UnsupportedOperationException when the sensor in the desired direction is inactive
	 * @exception ArrayIndexOutOfBoundsException when the scanning goes into the infinite beyond. This is used and raised when the robot is looking through the exit
	 * and into the area outside the maze boundries. 
	 * @param Direction the relative direction to the orientation of the robot in which to detect a wall.
	 */
	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException, ArrayIndexOutOfBoundsException {
		if(!(this.hasOperationalSensor(direction))){
			throw new UnsupportedOperationException("The robot cannot see in that direction!"); //throw exception if no sensor
		}
		CardinalDirection scanningDirection = CardinalDirection.North; //temporary setting to north before changing it to the relevant direction
		switch(direction){
		case FORWARD:
			scanningDirection = this.getCurrentDirection();
			break;
		case BACKWARD:
			scanningDirection = this.getCurrentDirection().oppositeDirection();
			break;
		case RIGHT:
			scanningDirection = this.getCurrentDirection().oppositeDirection().rotateClockwise(); //current direction + 180 + 90 clockwise (right)
		case LEFT:
			scanningDirection = this.getCurrentDirection().rotateClockwise();
		}
		battery--; //decrement the battery for sensing in a given direction
		int[] dirPrime = scanningDirection.getDirection();
		for(int i = 0;; i++){
			try {
				if(floor.hasWall((this.getCurrentPosition()[0] + (i* dirPrime[0])), (this.getCurrentPosition()[1] + (i*dirPrime[1])), scanningDirection)){
					return i; //add i times the directional modifier each time to the current position, if there is a wall there then return false
				}
			} catch (ArrayIndexOutOfBoundsException OOB){
				return -1;
			} catch (Exception e) {
				//if this happens your robot is outside the maze, so you've won or you're hacking
			}
		}
	}
	
	/**This method returns the status of the sensor in the passed direction. True if the sensor is active, else it is false.
	 */
	@Override
	public boolean hasOperationalSensor(Direction direction) {
		switch(direction){
			case BACKWARD:							//checks if the value in given direction is equal to 1 (active)
				return (directionSensors[0] == 1);
			case FORWARD:
				return (directionSensors[1] == 1);
			case LEFT:
				return (directionSensors[2] == 1);
			case RIGHT:
				return (directionSensors[3] == 1);
		}
		return false;
	}

	/**This method triggers a temporary sensor failure in the passed direction. 
	 * This method will not cause a failure if the sensor is not currently active already.
	 * @param Direction is the relative direction to the current perspective that should have its sensor disabled.
	 */
	@Override
	public void triggerSensorFailure(Direction direction) {
		switch(direction){
		case BACKWARD:					//sets value in given direction to 0 (inactive)
			if(directionSensors[0] == 1)
				directionSensors[0] = 0;
			break;
		case FORWARD:
			if(directionSensors[1] == 1){
				directionSensors[1] = 0;
			}
			break;
		case LEFT:
			if(directionSensors[2] == 1){
				directionSensors[2] = 0;
			}
			break;
		case RIGHT:
			if(directionSensors[3] == 1){
				directionSensors[3] = 0;
			}
			break;
		}
	}

	/**This method restores the damaged sensors that were disabled by the triggerSensorFailure(Direction) method.
	 * This method will not restore sensors that the robot was not initalized with. 
	 * @param Direction the relative direction from the current persepctive that requires sensor repair.
	 */
	@Override
	public boolean repairFailedSensor(Direction direction) {
		switch(direction){
			case BACKWARD:
				if(directionSensors[0] == 0){		//if sensor was previously inactive but not disabled
					directionSensors[0] = 1;		//repair the sensor
				}									//if the sensor is disabled (-1) the return is false and no changes are made
				return (directionSensors[0] == 1);	//if the sensor is active, return true
				
			case FORWARD:
				if(directionSensors[1] == 0){		
					directionSensors[1] = 1;		
				}
				return (directionSensors[1] == 1);
			case LEFT:
				if(directionSensors[2] == 0){		
					directionSensors[2] = 1;	
				}
				return (directionSensors[2] == 1);
			case RIGHT:
				if(directionSensors[3] == 0){		
					directionSensors[3] = 1;		
				}
				return (directionSensors[3] == 1);
		}
		return false; //if none of the switch cases activate, return false
	}

	/**This method controls the rotation of the robot when attempting to turn. 
	 * @param Turn is the direction relative to the current persepective that the robot should turn.
	 */
	@Override
	public void rotate(Turn turn) {
		switch(turn){
		case LEFT:
			controller.keyDown(UserInput.Left, 0);
			break;
			
		case RIGHT:
			controller.keyDown(UserInput.Right, 0);
			break;
			
		case AROUND:
			controller.keyDown(UserInput.Right, 0);	//two turns in the same direction is the same as a 180 turn
			controller.keyDown(UserInput.Right, 0);		
			break;
		}
		return;
	}

	/**This method controls the movement of the robot, if it is in manual mode then the robot will not stop when it hits walls. 
	 * If it is automatic mode (manual == false) then the robot wil stop when it his a wall as well as setting the battery level to zero.
	 * @param distance is the number of spaces to move
	 * @param manual boolean that determines if the robot is in manual or automatic movement modes.
	 */
	@Override
	public void move(int distance, boolean manual) {
		for(; (hasStopped==false)&&(distance > 0); distance--){ //break loop if distance is covered or if the robot hits a roadblock
			try {
				if(floor.hasWall(this.getCurrentPosition()[0], this.getCurrentPosition()[1], this.getCurrentDirection())){ 
					if(manual){ //if there is a wall in manual mode, ignore it and do nothing
						this.hasStopped = false;
					}
					else { //if there is a wall in the way during automated drive mode then stop the robot
						this.hasStopped = true;
						this.battery = 0.0f;
					}
				}
				else{
					controller.keyDown(UserInput.Up, 1);//attempts to walk one space in the maze
				}
			} catch (Exception e) {
				// throws if the robot is outside of the maze
				e.printStackTrace();
			}
		}
	}

	/**This method deals with the tobot jumping over walls. Takes no parameters and will throw an exception if the robot attempts to jump an external wall.
	 */
	@Override
	public void jump() throws Exception {
		CardinalDirection direction = this.getCurrentDirection();
		int[] cordinates = this.getCurrentPosition();
			if(floor.hasWall(cordinates[0], cordinates[1], direction)){	//if there is a wall in the given direction from this position
				int[] cordinatesPrime = direction.getDirection();	
				if(((cordinates[0] + cordinatesPrime[0])>= maze.getWidth())||((cordinates[1] + cordinatesPrime[1])>= maze.getHeight())){
						throw new Exception("Jump would take you outside the maze");	//if the jump would put either the X or Y out of bounds of the maze, then return and do not jump
				}	
			}
			controller.keyDown(UserInput.Jump, 1);	//jumps
		}
	
	public void setController(Controller c){
		this.controller = c;
	}
}


