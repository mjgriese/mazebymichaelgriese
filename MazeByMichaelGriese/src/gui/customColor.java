package gui;

/**This class replaces the java.awt.color class and implements a few methods that are used in this codebase. 
 * The object is just an rgb int array.
 * @author Michael Griese
 */
public class customColor {

	public static final customColor yellow = new customColor(255, 255, 0);
	public static final customColor white =  new customColor(255, 255, 255);
	public static final customColor gray = new customColor(153, 153, 153);
	public static final customColor red = new customColor(255, 0, 0);
	public static final customColor darkGray = new customColor(102, 102, 102);
	public static final customColor black = new customColor(0, 0, 0);
	
	private int[] color = {0, 0, 0};
	
	/**Constructor for customColor 
	 * @param red 0- 255 value for red
	 * @param green 0-255 value for green
	 * @param blue 0-255 value for blue
	 * These three colors combine to create the overall color in the sRGB colorspace.
	 */
	public customColor(int red, int green, int blue) {
		color[0] = red;
		color[1] = green;
		color[2] = blue;
	}

	/**Encodes the color object into an integer representation
	 */
	public int getRGB() {		//literal integer of the rbg color value i think
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**This method returns a customColor object that reflects the integer that is passed to the method. 
	 * The integer is an encoding of the values of RGB 
	 * B bits 0-7
	 * G bits 8-15
	 * R bits 16-23
	 */
	public static customColor getColor(int col) {		//maybe do this stuff using shifts?
		int r = 0,g = 0,b = 0;
		int[] bitArray = new int[24];
		
		for(int i = 23; i > -1; i--){
			bitArray[i] = (col & (1 << i));
		}
		for(int i = 0; i < 8; i++){
			if(bitArray[i] != 0){
				b += Math.pow(2, i);
			}
		}
		for(int i = 0; i < 8; i++){
			if(bitArray[i + 8] != 0){
				g += Math.pow(2, i);
			}
		}
		for(int i = 0; i < 8; i++){
			if(bitArray[i + 16] != 0){
				r += Math.pow(2, i);
			}
		}
		return new customColor(r, g, b);
	}

	/**Retuns the red component of the color object
	 */
	public int getR() {
		return color[0];
	}
	/**Retuns the green component of the color object
	 */
	public int getG() {
		return color[1];
	}
	/**Retuns the blue component of the color object
	 */
	public int getB() {
		return color[2];
	}

}
