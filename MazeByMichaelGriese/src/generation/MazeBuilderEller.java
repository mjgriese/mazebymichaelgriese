package generation;

import java.util.ArrayList;

public class MazeBuilderEller extends MazeBuilder implements Runnable {
	/* things that you already have from your super class
	 *  width, height (int)
	 *  startx, starty (int) -- internal starting position for the user/exit detector
	 * 
	 * floorplan -- floorplan object to manipulate 
	 * dists -- distance matrix object 
	 * 
	 * random -- random number generator
	 * 
	 * */
	private int[][] setTracker; //this holds the set value of any given cell in the maze
	private int setCounter = 1;
	
	/**
	 * Constructor for randomly generated maze, no seed used.
	 */
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	/**
	 * Constructor used for deterministic generation of the maze, the same seed is used each time 
	 */
	public MazeBuilderEller(boolean det){
		super(det);
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	
	/**
	 * This method overrides the general MazeBuilder generation and implements Eller's algorithm. It scans through the maze
	 * one layer at a time and then scans within each layer to access each cell in the maze. The maze is created through reductive construction
	 * as each wallboard is removed and the remaining members create the maze as it is played.s
	 */
	@Override //place generation code in this method 
	protected void generatePathways() {
		setTracker = new int[width][height];
		for(int i = 0; i < width - 1; i++){//runs for all bust last row
			assignSets(i); //assign any cell not in a set to be a part of a set
			wallGeneration(i); //do the randomized wall generation 
			bottomwallGeneration(i); //select which of the current cells will continue to the next row
		}
		finalRowGeneration();
	}
	
	
	protected void assignSets(int i){
		for(int j = 0; j < height; j++){
			if(setTracker[i][j] == 0)	//if there is no value in the cell, give it one
				setTracker[i][j] = setCounter;
			setCounter ++;
		}
	}
	
	protected void wallGeneration(int i){
		for(int j = 0; j < height - 1; j++){
			int randomInt = random.nextInt();
			if(setTracker[i][j] == setTracker[i][j+1]){ //if the two cells next to one another are the  same set then do not remove the wall between them
				continue;
			}
			else{ //if the two cells are in different sets
				if(randomInt % 2 == 0){	//remove the wall between them and combine their sets
					Wallboard wall = new Wallboard(i, j, CardinalDirection.South);
					floorplan.deleteWallboard(wall);
					setTracker[i][j+1] = setTracker[i][j];
				}
			}
		}
	}
	
	protected void bottomwallGeneration(int i){
		int currentSet = 0;
		for(int j = 0; j < height; j++){
			ArrayList<Integer> possibleIndex = new ArrayList<Integer>();
			if(setTracker[i][j] == currentSet){
				continue;
			}
			currentSet = setTracker[i][j];
			for(int k = j; (k < height)&&(setTracker[i][k] == currentSet); k++){	//continue the scan looking only at the current set elements
				possibleIndex.add(k);
			}
			int indexInt = random.nextIntWithinInterval(0, possibleIndex.size()-1); //picks a random index of the generated list
			Wallboard wall = new Wallboard(i, j + indexInt, CardinalDirection.East); //generates a wall at that location+direction
			floorplan.deleteWallboard(wall);										//removes the wall
			setTracker[i + 1][j + indexInt] = setTracker[i][j]; 	//make the cell that is now open a member of the set
		}
	}
	
	protected void finalRowGeneration(){
		int i = width - 1;
		for(int j = 0; j < height - 1; j++){
			if(setTracker[i][j] != setTracker[i][j+1]){
				Wallboard wall = new Wallboard(i, j, CardinalDirection.South);
				floorplan.deleteWallboard(wall);
				setTracker[i][j+1] = setTracker[i][j];
			}
		}
	}

}
	

	
			

	

