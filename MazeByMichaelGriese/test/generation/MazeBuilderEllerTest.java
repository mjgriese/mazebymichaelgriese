package generation;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

public class MazeBuilderEllerTest {
	
	private orderStub testOrder;
	private Factory testFactory;
	private Maze testMaze;
	private int desiredSkill = 5;
	private boolean isPerfect = false;
	private Distance testDistance;
	
	@Before
	public void setUp(){
		testFactory = new MazeFactory(true);
		testOrder = new orderStub(desiredSkill, isPerfect, true);
		testFactory.order(testOrder); 
		testFactory.waitTillDelivered();
		testMaze = testOrder.getMaze();
		testDistance = testMaze.getMazedists();
	}
	
	/**This test checks if a maze object is returned from the factory as requested
	 */
	@Test
	public final void testMazeReturn(){ //tests to see if the maze object was generated at all and is available for more testing
		assertNotNull(testMaze); 
		assertTrue(testMaze instanceof Maze);
	}
	
	
	/**This test checks for the exit of the maze and confirms that it exists
	 */
	@Test
	public final void testHasExit(){		//tests to see if there is an exit to the maze
		int [] startPosition = testDistance.getStartPosition();
		assertNotNull(testMaze.getDistanceToExit(startPosition[0], startPosition[1]));
	}

	/**This test shows that the exit can be reached from every cell(location) in the maze.
	 * It is done by iterating through the maze and checking the distance from the current cell to the end. If the value is not the max integer 
	 * value, then the maze is functional as every cell is set to that value during the set up process. 
	 */
	@Test
	public final void testCanAlwaysExit(){	//tests every space in the maze to see if the exit is reach able from that location
		int width = testMaze.getWidth();
		int height = testMaze.getHeight();
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				assertTrue(testMaze.getDistanceToExit(i,j) < Integer.MAX_VALUE);
			}
		}
	}
	
	/**This test uses the auto complete method for the maze and determines if the method properly returns the nearby cell that is closer to the exit 
	 * than the current or starting cell.
	 */
	@Test
	public final void testAutoComplete(){ //test to see if the auto completion method for the maze returns a lower number (one step closer)
		int x = 0, y = 0;
		int [] primeLocation = testMaze.getNeighborCloserToExit(x, y);
		assertTrue(testMaze.isValidPosition(x, y));
		assertTrue(testMaze.getDistanceToExit(x,y) > testMaze.getDistanceToExit(primeLocation[0],primeLocation[1]));
	}
	
	/**This method generates two mazes from two factories but both using the same seed for generation; meaning that they should have the same floorplans 
	 * and the same internal structure.
	 */
	@Test
	public final void testDeterministicGeneration(){ 
		orderStub secondtestOrder = new orderStub(desiredSkill, isPerfect, true);
		Factory secondtestFactory = new MazeFactory(true);
		secondtestFactory.order(secondtestOrder);
		secondtestFactory.waitTillDelivered();
		Maze secondtestMaze = secondtestOrder.getMaze();
		assertTrue(secondtestMaze.getFloorplan() == testMaze.getFloorplan());
	}
	
	
	
	/* I don't think I will need to use this but hey why not include it?
	@After
	public void tearDOwn(){
	}
	*/
	
}
