package generation;

public class orderStub implements Order {
		private boolean isPerfect;
		private int skillLevel;
		private int precentComplete = 0;
		private Builder buildType; //holds general information about the order 
		private Maze generatedMaze; 
		
		public orderStub(int skillLevel, boolean isPerfect, boolean testEller){
			this.skillLevel = skillLevel;
			this.isPerfect = isPerfect;
			if(testEller)
				this.buildType = Builder.Eller;
			else
				this.buildType = Builder.Prim;
		}
		@Override
		public boolean isPerfect() {
			return this.isPerfect; 
		}
		@Override
		public int getSkillLevel(){
			return this.skillLevel;
		}
		@Override
		public Builder getBuilder(){
			return this.buildType;
		}
		@Override
		public void updateProgress(int precent){
			this.precentComplete = precent;
		}
		@Override
		public void deliver(Maze generatedMaze){
			this.generatedMaze = generatedMaze;
			System.out.println("Order has been fulfilled") ;
		}
	
		public Maze getMaze(){
			return this.generatedMaze;
		}
	}