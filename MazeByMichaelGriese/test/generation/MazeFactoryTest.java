package generation;

import static org.junit.Assert.*;
import generation.CardinalDirection; //idk about these imports but I think they are going to be useful 
import generation.Floorplan;

import org.junit.Before;
import org.junit.Test;

public class MazeFactoryTest {
		
	private orderStub testOrder;
	private Factory testFactory;
	private Maze testMaze;
	private int desiredSkill = 5;
	private boolean isPerfect = false;
	private Floorplan testFloorplan;
	private Distance testDistance;
	
	@Before
	public void setUp() {
		testFactory = new MazeFactory(true);
		testOrder = new orderStub(desiredSkill, isPerfect, false);
		testFactory.order(testOrder);
		testFactory.waitTillDelivered();
		testMaze = testOrder.getMaze();
		testFloorplan = testMaze.getFloorplan();
		testDistance = testMaze.getMazedists();
	}
	
	/**This test checks if a maze object is returned from the factory as intended
	 */	
	@Test
	public final void testMazeReturn(){ //tests to see if the maze was properly generated and is available for more testing
		assertNotNull(testMaze); 
		assertTrue(testMaze instanceof Maze);
	}

	/**This test checks if there is an exit that is reachable from the starting position 
	 */
	@Test
	public final void testHasExit(){		//tests to see if there is an exit to the maze
		int [] startPosition = testDistance.getStartPosition();
		assertNotNull(testMaze.getDistanceToExit(startPosition[0], startPosition[1]));
	}

	/**This test determines if the exit is reachable from every cell in the maze by checking the distance at each cell and asserting that its value 
	*is no longer the max int value as it it once set 
	*/
	@Test
	public final void testCanAlwaysExit(){	//tests every space in the maze to see if the exit is reach able from that location
		int width = testMaze.getWidth();
		int height = testMaze.getHeight();
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				assertTrue(testMaze.getDistanceToExit(i,j) < Integer.MAX_VALUE);
			}
		}
	}
	
	/**This test checks the auto complete method and asserts that it does indeed find a cell that is one step closer to the exit
	 */
	@Test
	public final void testAutoComplete(){ //test to see if the auto completion method for the maze returns a lower number (one step closer)
		int x = 0, y = 0;
		int [] primeLocation = testMaze.getNeighborCloserToExit(x, y);
		assertTrue(testMaze.isValidPosition(x, y));
		assertTrue(testMaze.getDistanceToExit(x,y) > testMaze.getDistanceToExit(primeLocation[0],primeLocation[1]));
	} 

	/**This test iterates through the cells of the maze and if a wall is seen then a +1 is added to a counter
	 * if at least 1/5 of the possible walls are present, the test passes. This is to dissuade puzzles that are too easy or straightforward
	 */
	@Test
	public final void testWallCount(){
		int width = testMaze.getWidth();
		int height = testMaze.getHeight();
		int x = 0;
		Floorplan floor = testMaze.getFloorplan();
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				for(CardinalDirection dir: CardinalDirection.values()){
					if(floor.hasWall(i, j, dir)){
						x++;
					}
				}
			}
		}
		assertTrue((width * height * 4/5) < x);
	}



}








