package gui;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

import generation.Maze;
import gui.Robot.Direction;

public class WallFollowerTest {

	private Maze testMaze;
	private BasicRobot robot;
	private Controller controller;
	private RobotDriver robotDriver;
	
	@Before
	public void setUp(){
		robot = new BasicRobot();	//creates robot with 3k enerygy and all sensors active
		controller = new Controller();
		robotDriver = new WallFollower();
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating("C:\\Users\\Michael\\git\\mazebymichaelgriese\\MazeByMichaelGriese\\test\\data\\input.xml");
		robot.setMaze(controller);					//uses the same maze every time 
		robotDriver.setRobot(robot);
		controller.setRobotAndDriver(robot, robotDriver);
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		testMaze = controller.getMazeConfiguration();
	}
	
	/**This is a sloppy test that simply gives a maze to the robot and has it drive around to solve it.
	 * Requires 50 seconds to complete the standard maze.
	 */
	@Test
	public final void testMovement() throws Exception{
		int cords[] = robot.getCurrentPosition();
		assertTrue(cords[0] == 4);
		assertTrue(cords[1] == 0);
		assertTrue(robotDriver.drive2Exit());
		cords = robot.getCurrentPosition();
		assertTrue(cords[0] == 0);
		assertTrue(cords[1] == 3);
	}
	
	@Test //these are the only two sensors really tested because they are the only ones that are used for the alg. 
	public final void testAltSensorsForward() {
		robot.triggerSensorFailure(Direction.FORWARD);
		robotDriver.triggerUpdateSensorInformation();
		int distAhead = ((WallFollower) robotDriver).lookAtWall(Direction.FORWARD);
		assertTrue(11 == distAhead);
		assertFalse(((WallFollower) robotDriver).getCurrentSensors()[2]); 
		assertTrue(robot.getBatteryLevel() == 3000 - 7);
	}
	
	@Test
	public final void testAltSensorsLeft() {
		robot.triggerSensorFailure(Direction.LEFT);
		robotDriver.triggerUpdateSensorInformation();
		int distAhead = ((WallFollower) robotDriver).lookAtWall(Direction.LEFT);
		assertTrue(0 == distAhead);
		assertFalse(((WallFollower) robotDriver).getCurrentSensors()[0]); 
		assertTrue(robot.getBatteryLevel() == 3000 - 7);
	}
}
