package gui;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import generation.Maze;

public class WizzardTest {

	private Maze testMaze;
	private BasicRobot robot;
	private Controller controller;
	private RobotDriver robotDriver;
	
	@Before
	public void setUp(){
		robot = new BasicRobot();	//creates robot with 3k enerygy and all sensors active
		controller = new Controller();
		robotDriver = new Wizard();
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating("C:\\Users\\Michael\\git\\mazebymichaelgriese\\MazeByMichaelGriese\\test\\data\\input.xml");
		robot.setMaze(controller);					//uses the same maze every time 
		robotDriver.setRobot(robot);
		controller.setRobotAndDriver(robot, robotDriver);
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		testMaze = controller.getMazeConfiguration();
		robotDriver.setDistance(testMaze.getMazedists());
	}
	
	@Test
	public final void testMovement() throws Exception{
		int cords[] = robot.getCurrentPosition();
		assertTrue(cords[0] == 4);
		assertTrue(cords[1] == 0);
		robotDriver.drive2Exit();
		cords = robot.getCurrentPosition();
		assertTrue(cords[0] == 0);
		assertTrue(cords[1] == 3);
	}
	
	@Test 
	public final void testBatteryReturn() throws Exception{
		robotDriver.drive2Exit();
		assertTrue(150 == robotDriver.getEnergyConsumption());
	}
}
