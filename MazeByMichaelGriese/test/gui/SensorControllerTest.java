package gui;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Before;
import org.junit.Test;

import generation.Maze;
import gui.Robot.Direction;

public class SensorControllerTest {


	private Maze testMaze;
	private BasicRobot robot;
	private Controller controller;
	private RobotDriver robotDriver;
	
	@Before
	public void setUp(){
		robot = new BasicRobot();	//creates robot with 3k enerygy and all sensors active
		controller = new Controller();
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating(".\\test\\data\\input.xml");
		robot.setMaze(controller);					//uses the same maze every time 
		controller.setRobotAndDriver(robot, robotDriver);
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		testMaze = controller.getMazeConfiguration();
	}
	
	@Test
	public final void testControlerManipulation(){
		controller.toggleSensorStability(Direction.RIGHT);
		assertFalse(controller.getStableList()[1]);
	}
	
	@Test 
	public final void testRobotInterference() throws InterruptedException{
		controller.toggleSensorStability(Direction.RIGHT);
		while(controller.getSensorControl().getDamage() == false){
			Thread.sleep(100);
		}
		assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(Direction.RIGHT);});
	}
}
