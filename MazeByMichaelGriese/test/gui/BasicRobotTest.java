package gui;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

import generation.CardinalDirection;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;
/**Colaborates with the BasicRobot class to preform diagnostic tests and ensure the class is operation properly. 
 * Has 96.1% Coverage! Good work bud.
 * @author Michael Griese
 */
public class BasicRobotTest {

	private Maze testMaze;
	private BasicRobot robot;
	private Controller controller;
	private RobotDriver robotDriver;
	
	@Before
	public void setUp(){
		robot = new BasicRobot();	//creates robot with 3k enerygy and all sensors active
		controller = new Controller();
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating("C:\\Users\\Michael\\git\\mazebymichaelgriese\\MazeByMichaelGriese\\test\\data\\input.xml");
		robot.setMaze(controller);					//uses the same maze every time 
		controller.setRobotAndDriver(robot, robotDriver);
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		testMaze = controller.getMazeConfiguration();
	}
	
	
	/**This test determines if the controller and robot are properly associated and contrain references to eachother
	 */
	@Test
	public final void testSetMazeAssociation(){
		assertTrue(robot.controller == this.controller);
		assertTrue(controller.robot == this.robot);
		}
	
	/**This test determines of the robot and controller return the same coordinate address of the current position of the robot
	 */
	@Test
	public final void testPosition() throws Exception{
		int[] robotCords = robot.getCurrentPosition();
		int[] referenceCords = controller.getCurrentPosition();
		assertTrue(robotCords[0] == referenceCords[0]);
		assertTrue(robotCords[1] == referenceCords[1]);
		
		controller.setPositionOutOfBounds();
		assertThrows(Exception.class, () -> {robot.getCurrentPosition();});
	}
	/**This test determines if the cardinal direction that is returned from the robot is the same as the one that is returned from the Controller.
	 */
	@Test
	public final void testCardinalDirection(){
		CardinalDirection robotDir = robot.getCurrentDirection();
		CardinalDirection controllerDir = controller.getCurrentDirection();
		assertTrue(robotDir == controllerDir);
	}
	
	/**This test determines if the battery is initalized to the proper value and if the setBattery method works properly. 
	 */
	@Test
	public final void testBatteryReturn(){
		assertTrue(3000.0f == robot.getBatteryLevel());
		robot.setBatteryLevel(100.0f);
		assertTrue(100.0f == robot.getBatteryLevel());
	}
	
	
	/**This method tests the odometer functionality by checking the intialized value, moving the robot once space and checking, and then resetting the
	 * odometer before checking its value once again.
	 */
	@Test
	public final void testOdometer(){
		assertTrue(0 == robot.getOdometerReading());
		robot.move(1, true);
		assertTrue(1 == robot.getOdometerReading());
		robot.resetOdometer();
		assertTrue(0 == robot.getOdometerReading());
	}
	
	/**Tests if the robot is at an exit position and compares it to the floorplan exit value at the current position. 
	 * @throws Exception if the current position is out of the boundries of the maze
	 */
	@Test
	public final void testExitDetection() throws Exception{
		assertTrue(robot.isAtExit() == testMaze.getFloorplan().isExitPosition(robot.getCurrentPosition()[0], robot.getCurrentPosition()[1]));
		controller.setPositionOutOfBounds();
		assertFalse(robot.isAtExit());
	}
	
	/**This test determines if the robot can detect if it is inside of a room within the maze. It raises an UnsupportedOperationException if 
	 * there is no valid room sensor. The return type is false if the robot is outside of the maze boundires.
	 */
	@Test
	public final void testRoomDetection() throws UnsupportedOperationException, Exception{
		robot.roomSensor = true;
		assertTrue(robot.hasRoomSensor());
		assertTrue(robot.isInsideRoom() == testMaze.getFloorplan().isInRoom(robot.getCurrentPosition()[0], robot.getCurrentPosition()[1]));
		robot.roomSensor = false;
		assertFalse(robot.hasRoomSensor());
		assertThrows(UnsupportedOperationException.class, () -> {robot.isInsideRoom();});
		
		robot.roomSensor = true;
		controller.setPositionOutOfBounds();
		assertFalse(robot.isInsideRoom());
	}
	
	/**Tests if the correct value is returned for the value of a full rotation
	 */
	@Test
	public final void testRotationEnergyCost(){
		assertTrue(robot.getEnergyForFullRotation() == 12.0F);
	}
	
	/**Tests if the correct value is returned for the value of moving one space within the maze
	 */
	@Test
	public final void testMoveEnergyCost(){
		assertTrue(robot.getEnergyForStepForward() == 5.0F);
	}
	
	/**Tests if the correct value is returned from the method
	 */
	@Test
	public final void testHasStopped(){
		assertTrue(robot.hasStopped() == robot.hasStopped);
	}
	
	/**Tests the sensor disabling and repair. For each direction, the sensor is tests to see if it is initalized properly before being disabled. 
	 * Each direction is then tested to ensure the exception is thrown 
	 * Each direction is then repaired and then again tested to ensure the repair was successful.
	 */
	@Test
	public final void testSensorManipulation(){
		for(Direction d : Direction.values()){
			assertTrue(robot.hasOperationalSensor(d));	//test to see if they initalize
			robot.triggerSensorFailure(d);				//break them 
		}
		for(Direction d : Direction.values()){
			assertFalse(robot.hasOperationalSensor(d));	//test to see if they borke
			robot.repairFailedSensor(d);				//fix them
		}
		for(Direction d : Direction.values()){
			assertTrue(robot.hasOperationalSensor(d));	//see if they got fixed
		}
	}
	
	/**Tests if the automatic mode will stop the robot if it hits a wall. The robot should also have 0 battery after hitting the wall.
	 */
	@Test 
	public final void testMovementAutomatic(){		
		robot.move(12, false);
		assertTrue(robot.hasStopped == true);
		assertTrue(robot.getBatteryLevel() == 0);
	}
	
	@Test
	public final void testAutoMoveNoCollision(){
		robot.move(11, false);
		assertTrue(robot.hasStopped == false);
		assertTrue(robot.getBatteryLevel() == 3000 - 55);
	}
	
	
	/**Sets the battery level to a value below the movement threshold and then attempts to move. 
	 * hasStopped should be true as well as 0 battery
	 */
	@Test
	public final void testMovementNoBattery(){
		robot.setBatteryLevel(4.0f);
		robot.move(1, true);
		assertTrue(robot.hasStopped);
		assertTrue(robot.getBatteryLevel() == 0.0f);
	}
	
	/**Tests to see if the robot will continue operation if rammed into a wall in manual mode. 
	 * The robot is driven into a wall in manual mode and hasStopped should be false
	 * The battery level should NOT be reduced to zero but rather the ammount required to travel the set distance (11 units = 55 fuel)
	 */
	@Test 
	public final void testMovementManual(){
		robot.move(12, true);
		assertTrue(robot.hasStopped == false);
		assertTrue(robot.getBatteryLevel() == 3000.0f - 55);
	}
	
	/**Tests the turning aspects of the robot.
	 * The robot is spun around and then the result is compared to the inital direction.
	 * The battery level is also periodically checked. 
	 * The battery level is set to a value too small to turn twice. The turns are attempted and the robot should only be able to complete one of them.
	 * hasStopped should be true and the current direction should be only one turn away from the previous position. 
	 */
	@Test
	public final void testRotation(){
		CardinalDirection origDirection = robot.getCurrentDirection();
		for(Turn t: Turn.values()){
			robot.rotate(t);
		}	//left + right + around = only around turn
		assertTrue(origDirection.oppositeDirection() == robot.getCurrentDirection());
		assertTrue(robot.getBatteryLevel() == 3000-12);
		
		robot.setBatteryLevel(5);
		origDirection = robot.getCurrentDirection();
		robot.rotate(Turn.LEFT);
		robot.rotate(Turn.LEFT);
		assertTrue(robot.getCurrentDirection() == origDirection.rotateClockwise());
		assertTrue(robot.hasStopped);
	}
	
	/**The robot jumps in place and the cordiantes are checked to ensure movement happened.
	 * The robot then moves 10 more spaces to be in front of a wall before jumping again. 
	 * The same detection is used for the second jump to ensure movement through the wall. 
	 * The battery is set to a value too low to jump and then a jump is called.
	 * The robot should not move, battery == 0, and hasStopped == true
	 */
	@Test 
	public final void testJump() throws Exception{
		int[] prejumpCords = robot.getCurrentPosition();
		CardinalDirection moveDirection = robot.getCurrentDirection();
		int[] jumpPrime = moveDirection.getDirection();
		robot.jump();
		assertTrue(prejumpCords[0] + jumpPrime[0] == robot.getCurrentPosition()[0]);
		assertTrue(prejumpCords[1] + jumpPrime[1] == robot.getCurrentPosition()[1]);
		robot.move(10, true); //move 10 more spaces to be in front of a wall 
		prejumpCords = robot.getCurrentPosition();		//reset the variables to determine if we actually move or not
		
		moveDirection = robot.getCurrentDirection();
		jumpPrime = moveDirection.getDirection();
		robot.jump();
		assertTrue(prejumpCords[0] + jumpPrime[0] == robot.getCurrentPosition()[0]);
		assertTrue(prejumpCords[1] + jumpPrime[1] == robot.getCurrentPosition()[1]);
		
		robot.setBatteryLevel(40.0f);
		robot.jump();
		assertTrue(robot.hasStopped);
		assertTrue(robot.getBatteryLevel() == 0.0f);
	}
	
	/**The robot is telported within the maze to a location in front of a border wall and the jump is attempted. 
	 * The detection is based on the room sensors, before the jump the robot is not in a room but after it is in the room. 
	 */
	@Test 
	public final void testJumpingBorderWalls() throws Exception{
		//robot.rotate(Turn.RIGHT);
		//assertThrows(Exception.class, () -> {robot.jump();});
		//TODO: Determine how to check for outside jumping >:(
		//this second half tests if you can jump into rooms as intended
		controller.setPosition(7, 4);
		assertFalse(robot.isInsideRoom());
		robot.rotate(Turn.LEFT);
		robot.jump();
		assertTrue(robot.isInsideRoom());
	}
	
	/** The robot looks forward to the wall that is 11 units away and the value it returns is tested.
	 * The sensor is broken and the robot is told to look again, the exception is raised.
	 * The sensor is repaired and the robot is told to look again, and once again the result is compared to the known value.
	 */
	@Test 
	public final void testLookingAhead(){
		int distance = robot.distanceToObstacle(Direction.FORWARD);
		assertTrue(distance == 11);
		assertTrue(robot.getBatteryLevel() == 3000.0f -1);
		//cause failure in the sensor 
		robot.triggerSensorFailure(Direction.FORWARD);
		assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(Direction.FORWARD);});
		//restore sensor anc check just to be sure 
		robot.repairFailedSensor(Direction.FORWARD);
		distance = robot.distanceToObstacle(Direction.FORWARD);
		assertTrue(distance == 11);
	}
	
	/** The robot looks backward to the wall that is 0 units away and the value it returns is tested.
	 * The sensor is broken and the robot is told to look again, the exception is raised.
	 * The sensor is repaired and the robot is told to look again, and once again the result is compared to the known value.
	 */
	@Test 
	public final void testLookingBackward(){
		int distance = robot.distanceToObstacle(Direction.BACKWARD);
		assertTrue(distance == 0);
		assertTrue(robot.getBatteryLevel() == 3000.0f -1);
		//cause failure in the sensor 
		robot.triggerSensorFailure(Direction.BACKWARD);
		assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(Direction.BACKWARD);});
		//restore sensor anc check just to be sure 
		robot.repairFailedSensor(Direction.BACKWARD);
		distance = robot.distanceToObstacle(Direction.BACKWARD);
		assertTrue(distance == 0);
	}
	
	/** The robot looks left to the wall that is 0 units away and the value it returns is tested.
	 * The sensor is broken and the robot is told to look again, the exception is raised.
	 * The sensor is repaired and the robot is told to look again, and once again the result is compared to the known value.
	 */
	@Test
	public final void testLookingLeft(){
		int distance = robot.distanceToObstacle(Direction.LEFT);
		assertTrue(distance == 0);
		assertTrue(robot.getBatteryLevel() == 3000.0f -1);
		//cause failure in the sensor 
		robot.triggerSensorFailure(Direction.LEFT);
		assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(Direction.LEFT);});
		//restore sensor anc check just to be sure 
		robot.repairFailedSensor(Direction.LEFT);
		distance = robot.distanceToObstacle(Direction.LEFT);
		assertTrue(distance == 0);
	}
	
	/** The robot looks right to the wall that is 0 units away and the value it returns is tested.
	 * The sensor is broken and the robot is told to look again, the exception is raised.
	 * The sensor is repaired and the robot is told to look again, and once again the result is compared to the known value.
	 */	
	@Test
	public final void testLookingRight(){
		int distance = robot.distanceToObstacle(Direction.RIGHT);
		assertTrue(distance == 0);
		assertTrue(robot.getBatteryLevel() == 3000.0f -1);
		//cause failure in the sensor 
		robot.triggerSensorFailure(Direction.RIGHT);
		assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(Direction.RIGHT);});
		//restore sensor anc check just to be sure 
		robot.repairFailedSensor(Direction.RIGHT);
		distance = robot.distanceToObstacle(Direction.RIGHT);
		assertTrue(distance == 0);
	}
	
	/**The robot is telported to the exit position of the maze. It is told to look in every direction and they returns are compared to the known maze state. 
	 * The sensor is broken and the look is attempted again. The exception is raised. 
	 * The sensor is repaired and the look is attempted again, and the value is compared to the known maze state.
	 */
	@Test
	public final void testExitAbyssBackward(){
		controller.setPosition(0, 3);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD) == false);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD) == true);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.LEFT) == false);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT) == false);
		
		robot.triggerSensorFailure(Direction.BACKWARD);
		assertThrows(UnsupportedOperationException.class, () -> {robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD);});
				
		robot.repairFailedSensor(Direction.BACKWARD);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD) == true);
	}

	/**The robot is telported to the exit position of the maze. It is told to look in every direction and they returns are compared to the known maze state. 
	 * The sensor is broken and the look is attempted again. The exception is raised. 
	 * The sensor is repaired and the look is attempted again, and the value is compared to the known maze state.
	 */
	@Test
	public final void testExitAbyssForward(){
		controller.setPosition(0, 3);
		robot.rotate(Turn.AROUND);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD) == true);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD) == false);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.LEFT) == false);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT) == false);
		
		robot.triggerSensorFailure(Direction.FORWARD);
		assertThrows(UnsupportedOperationException.class, () -> {robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD);});
				
		robot.repairFailedSensor(Direction.FORWARD);
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD) == true);
	}
}

